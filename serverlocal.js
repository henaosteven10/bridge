var express = require("express");
var bodyParser = require("body-parser");
const fs = require("fs");
var cmd = require("node-cmd");
var archivos = require("./Funtions/savefile");
var app = express();

app.use(express.static("my-app/build"));
app.use(bodyParser());

app.post("/getcodexml",function(req,res){
  console.log(req.body.data);
  archivos.readFile(req.body.data,res);
  //res.send({michal:'perro'});
})
app.post("/deletexml",function(req,res){
  archivos.deleteFile(req.body.data);
  res.send({status:"deleted"});
})

app.get("/getnumberserie", function(req, res) {
  cmd.get("wmic bios get serialnumber", function(err, data, stderr) {
    if (!err) {
      res.send({ numberserie: data.split("\n") });
    } else {
      res.send({ numberserie: "error" });
      console.log(err);
    }
  });
});

app.post("/uploadino", function(req, res) {
  console.log("*****uploadino papa********");
  console.log(req.body);
  fs.writeFile(__dirname + "/primercode/primercode.ino", req.body.code, err => {
    if (err) throw err;
    console.log("Archivo actualizado Satisfactoriamente");
  });
  execute(res, req.body.portselect);
  //res.send({ respuesta: "michael" });
});
app.post("/savexml", function(req, res) {
  console.log(req.body);
  res.send({ status: "guardandoooooooo" });
  archivos.createFile(req.body.data.codeXml, req.body.data.nameFile);
});
app.get("/getfiles", function(req, ress) {
  console.log(req.body)
  archivos.existFile(function(err, res) {
    ress.send({ files: res })
  });
});
app.get("/getport", function(req, res) {
  // res.send('us y cuantos mas')
  //arduino-cli board list
  cmd.get("arduino-cli board list", function(err, data, stderr) {
    var row = data.split("\n");
    const port = [];

    row.forEach(element => {
      var col = element.trim().split(" ");
      if (col[0].indexOf("COM") != -1) {
        port.push(col[0]);
      }
    });
    if (!err) {
      res.send({ port: port });
    } else {
      res.send({ port: port });
      console.log(err);
    }
  });
});

function execute(res, port) {
  cmd.get("arduino-cli compile --fqbn arduino:avr:uno primercode", function(
    err,
    data,
    stderr
  ) {
    if (!err) {
      console.log("COMPILAD:", data);
      //res.send({respuesta:data});
      const compilad = data;
      cmd.get(
        "arduino-cli upload --port " +
          port +
          " --fqbn arduino:avr:uno primercode",
        function(err, data, stderr) {
          if (!err) {
            res.send({ status: compilad });
            console.log("the node-cmd cloned dir contains these files :", data);
          } else {
            const error = "Update - " + err;
            console.log(error);
            res.send({ status: error });
          }
        }
      );
    } else {
      console.log("error", err);
      res.send({ status: "Compiled - " + err });
    }
  });
}

var server = app.listen(process.env.PORT || 80, function() {
  console.log("LocalHost listening...");
});
