import React from "react";
import "./App.css";
import Main from "./Pages/PageMain"

class App extends React.Component{
  state = {
    channels:[],
    enable:false
  }
  componentDidMount = async () =>{
    try {
      let config = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify()
      };
      let response = await fetch(`/getnumberserie`, config);
      const res = await response.json();
      console.log("*** respuesta port ***");
      console.log(res);
      this.setState({channels:[res.numberserie[1].trim()],enable:true})
    } catch (error) {}
  }
  render(){
    return(
      <Main channels = {this.state.channels} enable={this.state.enable}/>
    )
  }
}
export default App;
