import React from "react";
import PubNub from "pubnub";
import Websocket from "../Components/websocket";

const pubnub = new PubNub({
  publishKey: "pub-c-c98dcb6c-e530-4918-aba2-a43eed570bfe",
  subscribeKey: "sub-c-b17f9298-64da-11ea-abb5-76a98e4db888",
  uuid: "12312423423423423423523314679789678ufghxdfwv4"
});
//const channels = ["awesomeChannel"];

// const options = [
//   { value: "COM19", label: "COM19" },
//   { value: "COM6", label: "COM6" },
//   { value: "COM3", label: "COM3" }
// ];

export default class Main extends React.Component {
  sendMessage = message => {
    pubnub.publish({
      channel: this.props.channels,
      message
    });
  };

  handlerWebsocket = async rx => {
    console.log(rx);
    if (rx.from === "pageweb") {
      if (rx.request === "port") {
        const data = { peticion: "port" };
        try {
          let config = {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify()
          };
          let response = await fetch(`/getport`, config);
          const res = await response.json(data);
          console.log("*** respuesta port ***");
          console.log(res);
          this.sendMessage({
            from: "localhost",
            request: false,
            res: "port",
            data: res.port
          });
        } catch (error) {}
      } else if (rx.request === "upload") {
        const data = { peticion: "upload" };
        try {
          let config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify(rx.data)
          };
          let response = await fetch(`/uploadino`, config);
          const res = await response.json(data);
          console.log("*** respuesta depus de upgadte***");
          console.log(res);
          this.sendMessage({
            from: "localhost",
            request: false,
            res: "upload",
            data: res.status
          });
        } catch (error) {}
      } else if (rx.request === "savecode") {
        try {
          let config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify(rx)
          };
          let response = await fetch(`/savexml`, config);
          const res = await response.json();
          console.log("*** respuesta depus de save***");
          console.log(res);
          this.sendMessage({
            from: "localhost",
            request: false,
            res: "saved",
            data: res.status
          });
        } catch (error) {}
      } else if (rx.request === "files") {
        const data = { peticion: "files" };
        try {
          let config = {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify()
          };
          let response = await fetch(`/getfiles`, config);
          const res = await response.json(data);
          console.log("*** respuesta files ***");
          console.log(res);
          this.sendMessage({
            from: "localhost",
            request: false,
            res: "files",
            data: res.files
          });
        } catch (error) {}
      } else if (rx.request === "xmlcode") {
        try {
          let config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify(rx)
          };
          let response = await fetch(`/getcodexml`, config);
          const res = await response.json();
          console.log("*** respuesta depus de sxml**");
          console.log(res);
          this.sendMessage({
            from: "localhost",
            request: false,
            res: "xmlcode",
            data: res.codexml
          });
        } catch (error) {}
      } else if (rx.request === "deletexml") {
        try {
          let config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify(rx)
          };
          let response = await fetch(`/deletexml`, config);
          const res = await response.json();
          console.log("*** respuesta depus de delete**");
          console.log(res);
        } catch (error) {}
      }
    }
  };
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <h1>.......Corriendo servidor local.......</h1>
        <h2>Canal de comunicacion: {this.props.channels}</h2>
        {this.props.enable ? (
          <Websocket
            local={this.handlerWebsocket}
            pubnub={pubnub}
            channels={this.props.channels}
          />
        ) : (
          <h1>preparando...</h1>
        )}
      </div>
    );
  }
}
