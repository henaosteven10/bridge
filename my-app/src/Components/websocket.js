import React from "react";
import { PubNubProvider, PubNubConsumer } from "pubnub-react";

class Websocket extends React.Component {
  state = {
    justSuscription: true
  };
  render() {
    return (
      <div>
        <PubNubProvider client={this.props.pubnub}>
          <PubNubConsumer>
            {client => {
              if (this.state.justSuscription === true) {
                client.addListener({
                  message: messageEvent => {
                    const rx = messageEvent.message;
                    if (rx.from === "pageweb") {
                      this.props.local(rx);
                    }
                  }
                });
                this.setState({ justSuscription: false });
                var channels = this.props.channels;
                client.subscribe({ channels });
                console.log("acabo el mensaje");
              }
            }}
          </PubNubConsumer>
        </PubNubProvider>
      </div>
    );
  }
}
export default Websocket;
