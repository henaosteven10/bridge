const fs = require("fs");

function existFile(out) {
  fs.readdir("./codeXml", out);
}
function readFile(name, res) {
  if (fs.existsSync("./codeXml/" + name)) {
    fs.readFile("./codeXml/" + name, "utf8", (err, data) => {
      if (err) throw err;
      res.send({ codexml: data });
    });
  } else {
    res.send({ codexml: null });
  }
}

function deleteFile(name) {
  if (fs.existsSync("./codeXml/" + name)) {
    fs.unlink("./codeXml/" + name, (err) => {
      if (err) throw err;
      console.log('Archivo Eliminado Satisfactoriamente');
    });
  }
}

function createFile(xml, name) {
  console.log("The path exists.");
  if (fs.existsSync("./codeXml/" + name + ".xml")) {
    fs.unlink("./codeXml/" + name + ".xml", err => {
      if (err) throw err;
      console.log("successfully deleted");
      fs.appendFile("./codeXml/" + name + ".xml", xml, err => {
        if (err) throw err;
        console.log("Archivo Creado Satisfactoriamente");
      });
    });
  } else {
    fs.appendFile("./codeXml/" + name + ".xml", xml, err => {
      if (err) throw err;
      console.log("Archivo Creado Satisfactoriamente");
    });
  }
}
exports.createFile = createFile;
exports.existFile = existFile;
exports.readFile = readFile;
exports.deleteFile = deleteFile;